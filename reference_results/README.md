Choose between data for the three different Reynolds numbers [100](Re_100), [1000](Re_1000), or [10000](Re_10000).
