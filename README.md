In this repository we gathered the computational results that were obtained in a study of the Kelvin-Helmholtz instability.
For details about the setup of the problem we refer to [ngsolve.org](http://www.ngsolve.org/kh-benchmark) and [1].

## Structure of the repository                
We distinguish two types of results here:
 * Our proposed "reference" solutions from [1]. You will find them in the directory [reference_results](reference_results).          
 * In [1] we also demonstrated effects of perturbed numerical solutions. For completeness we added the results [perturbation_results](perturbation_results).

Furthermore we provide simple scripts to generate plots from the given results in [how_to_plot](how_to_plot).

The main content are the [reference_results](./reference_results) which are subdivided by Reynolds number and mesh resolution. For the highest Reynolds number, results for varying polyonomial order of the used FE space are provided on the finest mesh.         
The whole directory structure is:
        
 * [perturbation_results](./perturbation_results)
   * [compiler_with_FMA](./perturbation_results/compiler_with_FMA)
   * [inaccurate_linear_solve](./perturbation_results/inaccurate_linear_solve)
   * [inaccurate_num_int](./perturbation_results/inaccurate_num_int)
   * [structured_trigs](./perturbation_results/structured_trigs)
   * [unstructured_trigs](./perturbation_results/unstructured_trigs)
 * [how_to_plot](./how_to_plot)
 * [reference_results](./reference_results)
     * [Re_100](./reference_results/Re_100)
       * [quads_128x128](./reference_results/Re_100/quads_128x128)
       * [quads_16x16](./reference_results/Re_100/quads_16x16)
       * [quads_256x256](./reference_results/Re_100/quads_256x256)
       * [quads_32x32](./reference_results/Re_100/quads_32x32)
       * [quads_64x64](./reference_results/Re_100/quads_64x64)
     * [Re_1000](./reference_results/Re_1000)
       * [quads_128x128](./reference_results/Re_1000/quads_128x128)
       * [quads_16x16](./reference_results/Re_1000/quads_16x16)
       * [quads_256x256](./reference_results/Re_1000/quads_256x256)
       * [quads_32x32](./reference_results/Re_1000/quads_32x32)
       * [quads_64x64](./reference_results/Re_1000/quads_64x64)
     * [Re_10000](./reference_results/Re_10000)
         * [quads_128x128](./reference_results/Re_10000/quads_128x128)
         * [quads_16x16](./reference_results/Re_10000/quads_16x16)
         * [quads_256x256](./reference_results/Re_10000/quads_256x256)
           * [k_refinement](./reference_results/Re_10000/quads_256x256/k_refinement)
           * [spectra](./reference_results/Re_10000/quads_256x256/spectra)
           * [vtk](./reference_results/Re_10000/quads_256x256/vtk)
         * [quads_32x32](./reference_results/Re_10000/quads_32x32)
         * [quads_64x64](./reference_results/Re_10000/quads_64x64)
        
## Data provided for each simulation        
Finally, to every simulation we provide the following data (some data is only provided for the finest resolutions).
### Time series data
The following integral (in space) quantities are provided:        
 * time (physical) 
 * time (scaled) 
 * kinetic energy
 * enstrophy
 * palinstrophy
 * L2 norm of div(u)
 * vorticity thickness
        
For the definitions of and explanations on these quantities we refer to [ngsolve.org](http://www.ngsolve.org/kh-benchmark) and [1].
        
The data is provided in two simple comma-separated values (csv) files in which every row corresponds to one (scaled) time unit in [0,..,400].
        
### Energy spectra
For some simulations energy spectra are also provided in a simple text file.
        
### Velocity/Vorticity snapshots
For our finest solution we provide velocity and vorticity as a field quantity sampled on a fine quadrilateral grid. The data is provided in terms of a vtk-file. You can visualize the solution with [ParaView](http://www.paraview.org).        

## References
[1] Philipp W. Schroeder, Volker John, Philip L. Lederer, Christoph Lehrenfeld, Gert Lube, Joachim Schöberl, "On reference solutions and the sensitivity of the 2D Kelvin–Helmholtz instability problem", [arXiv 1803:06893](https://arxiv.org/abs/1803.06893)